using System;

namespace AutoMapperDemo
{
    public class Aluno
    {
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public decimal Media { get; set; }
    }
}