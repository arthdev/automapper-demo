namespace AutoMapperDemo
{
    public class RegistrarAlunoDto
    {
        public string Nome { get; set; }
        public string Media { get; set; }
        public string DataNascimento { get; set; }
    }
}