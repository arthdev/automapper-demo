﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AutoMapperDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AlunoController : ControllerBase
    {
        private readonly IMapper _mapper;

        public AlunoController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult Post([FromBody] RegistrarAlunoDto dto)
        {
            var aluno = _mapper.Map<RegistrarAlunoDto, Aluno>(dto);
            return Ok(aluno);
        }
    }
}
