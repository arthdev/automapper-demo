using System;
using AutoMapper;
using System.Globalization;

namespace AutoMapperDemo.Mapping
{
    public class DateTimeConverter : ITypeConverter<string, DateTime>
    {
        public DateTime Convert(string sourceMember, DateTime destination, ResolutionContext context)
        {
            DateTime date = DateTime.ParseExact(
                sourceMember.Trim(),
                new string[] {
                    "yyyy'-'MM'-'dd' 'HH':'mm':'ss",
                    "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
                    "yyyy'-'MM'-'dd",
                },
                DateTimeFormatInfo.InvariantInfo,
                DateTimeStyles.RoundtripKind
            );

            return date;
        }
    }
}