using System;
using AutoMapper;

namespace AutoMapperDemo.Mapping
{
    public class AppProfile : Profile
    {
        public AppProfile()
        {
            CreateMap<string, decimal>().ConvertUsing<DecimalConverter>();
            CreateMap<string, DateTime>().ConvertUsing<DateTimeConverter>();
            CreateMap<RegistrarAlunoDto, Aluno>();
        }
    }
}