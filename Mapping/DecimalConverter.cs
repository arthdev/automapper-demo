using AutoMapper;
using System.Globalization;

namespace AutoMapperDemo.Mapping
{
    public class DecimalConverter : ITypeConverter<string, decimal>
    {
        public decimal Convert(string source, decimal destination, ResolutionContext context)
        {
            decimal number = decimal.Parse(
                source.Trim(),
                NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint,
                NumberFormatInfo.InvariantInfo
            );

            return number;
        }
    }
}